"use strict";
// Student obj
let student = {
  name: "",
  lastName: "",

  setName() {
    this.name = prompt("імя учня");
  },
  setLastName() {
    this.lastName = prompt("прізвище учня");
  },
  table: {},
};

// Name , Marks & lessons getter
student.setName();
student.setLastName();

let addedLesson;
let addedMark;
do {
  addedLesson = prompt("lesson name");
  if (addedLesson) {
    addedMark = +prompt("lesson mark");
    if (addedLesson && addedMark <= 12 && addedMark && !isNaN(addedMark)) {
      student.table[addedLesson] = addedMark;
    }
  }
} while (addedLesson);

// Marks calculating
let marks = Object.values(student.table);
let badMarksCount = 0;
let generalAmount = 0;
let marksCount = 0;
marks.forEach((mark) => {
  if (mark < 4) {
    badMarksCount += 1;
  }
  marksCount += 1;
  generalAmount += mark;
});
let averageMark = generalAmount / marksCount;

// Console write
if (!badMarksCount && marksCount) {
  console.log("student has beed graded & assigned to new course");
}
if (averageMark > 7) {
  console.log("scholarship is also assigned to student");
}
